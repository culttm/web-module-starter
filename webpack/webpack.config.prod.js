var webpack = require('webpack');

module.exports = {
    entry: {
        app:  './src/app.js'
    },
    output: {
        publicPath: '/',
        filename: '[name].min.js',
        path: 'dist/js'
    },
  module: {
    loaders: [
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components|libs)/,
            loader: 'babel'
        }
    ]
  },
  plugins: [
    new webpack.EnvironmentPlugin("NODE_ENV"),
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.ProvidePlugin({
      $: "window.jQuery",
    }),
    new webpack.optimize.UglifyJsPlugin({
        beautify: true,
        comments: false,
        compress: {
            sequences     : true,
            booleans      : true,
            loops         : true,
            unused      : true,
            warnings    : false,
            drop_console: true,
            unsafe      : true
        }
    })
  ]
}
