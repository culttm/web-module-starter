var webpack = require('webpack');

module.exports = {
  entry: {
    app:  './src/app.js'
  },
  output: {
    publicPath: '/',
    filename: '[name].js',
    path: 'dist/js'
  },
  watch: true,
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components|libs)/,
        loader: 'babel'
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "window.jQuery",
    }),
    new webpack.NoErrorsPlugin()
  ]
}
