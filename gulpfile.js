var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    mode = require('gulp-mode')({
        modes: ["production", "development"],
        default: "development",
        verbose: false
    }),
    rename = require('gulp-rename'),
    uglifycss = require('gulp-uglifycss');

gulp.task('sass', function () {
    return gulp.src('./sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: true
        }))
        .pipe(mode.production(uglifycss()))
        .pipe(mode.production(rename({
            suffix: ".min"
        })))
        .pipe(gulp.dest('./dist/css'));
});


gulp.task('sass:watch', function () {
    gulp.watch('./sass/**/*.scss', ['sass']);
});


gulp.task('default', ['sass', 'sass:watch']);
gulp.task('prod', ['sass']);
